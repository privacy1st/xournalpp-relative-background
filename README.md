# Relative Xournal++ background paths

Replace absolute paths of Xournal++ PDF backgrounds with relative ones.

## Installation

Save [the script](relative-xopp-background) under

- `~/.local/share/nautilus/scripts/relative-xopp-background` (for `GNOME Files`/`Nautilus`)
- `~/.local/share/nemo/scripts/relative-xopp-background` (for `Nemo`)

and make it executable:

- `chmod +x <script-path>`

Optional: Install [notify2](https://pypi.org/project/notify2/) and
[dbus-python](https://pypi.org/project/dbus-python/) with your package manager or with pip.

## Details

Here is an excerpt of a Xournal++ document `/home/user/notes/note1/paper.pdf.xopp`:

```xml
<?xml version="1.0" standalone="no"?>
<xournal creator="Xournal++ 1.1.0" fileversion="4">
    <title>Xournal++ document - see https://github.com/xournalpp/xournalpp</title>
    <!-- ... -->
    <page width="595.27600000" height="841.89000000">
        <background type="pdf" domain="absolute"
                    filename="/home/user/Documents/paper.pdf" pageno="1ll"/>
    <!-- ... -->
    </page>
    <!-- ... -->
</xournal>
```

If `/home/user/notes/note1/paper.pdf` is accessible (in the same folder as the Xournal++ file),
then the absolute path will be replaced with a path **relative** one:

```xml
<?xml version="1.0" standalone="no"?>
<xournal creator="Xournal++ 1.1.0" fileversion="4">
    <title>Xournal++ document - see https://github.com/xournalpp/xournalpp</title>
    <!-- ... -->
    <page width="595.27600000" height="841.89000000">
        <background type="pdf" domain="absolute"
                    filename="paper.pdf" pageno="1ll"/>
    <!-- ... -->
    </page>
    <!-- ... -->
</xournal>
```
